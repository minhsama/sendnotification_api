package com.minh.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.minh.dto.ApiResponse;
import com.minh.dto.GoogleApiRequest;
import com.minh.dto.PnsRequest;
import com.minh.service.FCMService;

@RestController
public class PushNotificationController {

	@Autowired
	private FCMService fcmService;
	
	@PostMapping("/notification")
	public String sendMessage(@RequestBody PnsRequest pnsRequest,@RequestParam String token) {
		return fcmService.pushNotification(pnsRequest,token);
	}
	
	@PostMapping("/notification/google_api")
	public ResponseEntity<ApiResponse> sendMessageGoogleApi(@RequestBody GoogleApiRequest apiRequest) {
		return fcmService.pushNotificationGoogleApi(apiRequest);
	}
}
