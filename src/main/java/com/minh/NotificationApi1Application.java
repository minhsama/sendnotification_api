package com.minh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotificationApi1Application {

	public static void main(String[] args) {
		SpringApplication.run(NotificationApi1Application.class, args);
	}

}
