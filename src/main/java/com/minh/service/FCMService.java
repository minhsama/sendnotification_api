package com.minh.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.Notification;
import com.google.gson.Gson;
import com.minh.dto.ApiResponse;
import com.minh.dto.GoogleApiRequest;
import com.minh.dto.PnsRequest;

@Service
public class FCMService {
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private final String currentDate = dateFormat.format(new Date());
	// send notification without google api
	public String pushNotification(PnsRequest pnsRequest,String token) {
		
		 Notification notification = Notification
	                .builder()
	                .setTitle(pnsRequest.getTitle())
	                .setBody(pnsRequest.getContent())
	                .setImage(pnsRequest.getImage())
	                .build();
		
		Message message = Message.builder()
				.putAllData(pnsRequest.getData())
				.setNotification(notification)
				.setToken(token)
				.build();
		
		String response = null;
		try {
			response = FirebaseMessaging.getInstance().send(message);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return response;
	}
	
	// send notification with google api "https://fcm.googleapis.com/fcm/send"
	public ResponseEntity<ApiResponse> pushNotificationGoogleApi(GoogleApiRequest apiRequest) {
		String result = new Gson().toJson(apiRequest);
		
		HttpPost httpPost = new HttpPost("https://fcm.googleapis.com/fcm/send");
		httpPost.addHeader("Authorization" , "key=AAAALVwm_I4:APA91bFNpE2rv03FKM5VXBN7d8Csie5V3nBFWf-ID3ahBZxS0qBvmMgQgiCGWI0UkpwV3QavMTZGWgeu0T94ESFusIWy4yuz9TVQUbpCSJznIhmR3eRcJVPOCJCr-H87M9jKsIF6gLFr");
		httpPost.addHeader("Content-Type" , "application/json");
		
		StringEntity stringEntity;
		try {
			stringEntity = new StringEntity(result);
			httpPost.setEntity(stringEntity);
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		CloseableHttpClient client = HttpClients.createDefault();
		
		
		
		try {
			HttpResponse response = client.execute(httpPost);
			return ResponseEntity.status(200).body(new ApiResponse(200,currentDate,"success",null,EntityUtils.toString(response.getEntity())));
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.status(200).body(new ApiResponse(200,currentDate,"success",null,null));
	}
	
}
