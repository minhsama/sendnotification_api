package com.minh.dto;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PnsRequest {
	private String title;
	private String image;
	private String content;
	private Map<String, String> data;
}
