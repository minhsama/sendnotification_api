package com.minh.dto;

import com.minh.model.Data;
import com.minh.model.Notification;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GoogleApiRequest {
	private String to;
	private Notification notification;
	private Data data;
}
